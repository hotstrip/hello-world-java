# 项目介绍

该项目是使用 Java 编写的一个 Hello world 项目，用于演示如何使用 Jpom 进行持续集成。

# 项目结构

```
├── README.md
├── pom.xml
└── src
    └── main
        └── java
            └── com
                └── jpom
                    └── demo
                        └── HelloWorld.java
                        └── HelloWorldTest.java
        └── resources
            └── application.propertis
```

# 功能介绍
功能就是一个简单的 http://localhost:8080/ 接口，返回 hello world 字符串。主要用来 Jpom 演示。